//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareOCUtility.h
//
// Created by    Noah Shan on 2018/11/13
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IIAPPNoNetDeclareOCUtility : NSObject


    - (NSString *)outPutDNSServers;

@end

NS_ASSUME_NONNULL_END
