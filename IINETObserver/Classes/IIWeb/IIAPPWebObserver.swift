//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPWebObserver.swift
//
// Created by    Noah Shan on 2018/10/25
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

/*
 当前APP进程级别网络监控
 1.父控制器展示UI
 2.进程级别Timer来处理网络连接状态
 3.时间段控制网络状态：
 a.连续两个相同的状态才代表一个真正的网络状态
 b.只有网络不好、没有网络、小助手才会提示，网络良好无需提示
 4.检测网络状态：
 a.[硬件状态]-系统API
 b.[DNS]-ping域名 & ip地址，一个通畅一个不通畅则为DNS问题
 c.[认证wifi]-访问域名，判定errorinfo-wifiURL是否为空
 5.Wifi认证状态：
 a.wifi正常
 b.wifi有小助手认证页面-需要跳转三级页面
 c.wifi没有认证页面，连接的是一个假的wifi

 再解释：
 关于errorcode = 4处理为redirect的说明
 一般情况：
 remote service 处理500一类或者404一类会返回html，解析json会报错errorcode=4
 此时也不会去处理errorinfo里面的errorurl
 处理情况：
 只有ping失败&wifi链接时，去请求接口返回errorcode=4识别为有wifi助手
 */

import Foundation

/// 通知名字
@objc public class IIAPPWebStatusNoti: NSObject {
    @objc public static let IIAPPWebStatusChangeNotiName = "IIAPPWebStatusChangeNotiName"
}

@objc public class IIAPPWebObserver: NSObject {

    /// ins
    @objc private static var shareInstance: IIAPPWebObserver?
    
    /// 状态日志
    public static var logAction: ((_ status: PingNetWorkStatus) -> Void)?

    /// 计时器
    @objc var each15SecTimer: Timer?

    /// 时间间隔
    private let timeInterval: TimeInterval = 7

    /// 最少连续2次状态相同才会更改UI
    private let equalsCount: Int = 2

    /// 默认为好状态
    var status: (status: PingNetWorkStatus, count: Int) = (PingNetWorkStatus.goodWork, 0)

    /// 通知发送的view
    var stateVW: IIAPPNoNetWorkVw?

    private override init() {
        super.init()
    }

    /// shareInstance
    @objc public static func getInstance() -> IIAPPWebObserver {
        if shareInstance == nil {
            shareInstance = IIAPPWebObserver()
        }
        return shareInstance!
    }

    /// 开启服务
    @objc public func startService() {
        IIHTTPNetWork.addObserver(selector: #selector(self.netWorkNotification(noti:)), observer: self)
        //RealReachability.sharedInstance().reachability(nil)
        each15SecProgressNet()
    }

    /// 停止服务
    @objc func stopService() {
        self.stopTimerService()
        NotificationCenter.default.removeObserver(self)
    }

    /// 计时器服务开启
    @objc func startTimerService() {
        each15SecTimer = Timer.scheduledTimer(timeInterval: timeInterval,
                                              target: self,
                                              selector: #selector(self.each15SecProgressNet),
                                              userInfo: nil,
                                              repeats: true)
    }

    /// 停止计时器服务
    @objc func stopTimerService() {
        self.each15SecTimer?.invalidate()
        self.each15SecTimer = nil
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension IIAPPWebObserver {

    /// 计时器处理问题【根据状态处理ui，如果检测到有网则结束计时器】
    @objc func each15SecProgressNet() {
        IIHTTPNetWork.getNetWorkStatusAndTimeLineWithPing { (status) in
            IIAPPWebObserver.logAction?(status)
            if status != .noNetWork {
                self.stopTimerService()
            }
            self.status.status = status
            self.progressUI()
        }
    }

    /// 网络监听,停止计时器服务 - 会进行二次认证 使用一个第三方再次进行认证 ; 双重否定才是否定
    @objc func netWorkNotification(noti: Notification) {
        self.stopTimerService()
        guard let reachability = noti.object as? RealReachability else { return }
        let status = reachability.currentReachabilityStatus()
        if status == ReachabilityStatus.RealStatusNotReachable || status == ReachabilityStatus.RealStatusUnknown {
            IIHTTPNetWork.getNetStatusWith3rd { (results) in
                if results {
                    self.netWorkNotiProgress(status: ReachabilityStatus.RealStatusViaWWAN)
                } else {
                    self.netWorkNotiProgress(status: ReachabilityStatus.RealStatusNotReachable)
                }
            }
        } else {
            self.netWorkNotiProgress(status: ReachabilityStatus.RealStatusViaWWAN)
        }
    }

    /// 网络监听处理：如果检测到有网，需要进行ping验证；没有网络的无需验证; 如果没有网络则开启每7秒一次的计时器
    /// unkonow为LTE & 3G & 2G 等
    /// - Parameter status: 状态
    func netWorkNotiProgress(status: ReachabilityStatus) {
        if status == .RealStatusNotReachable {
            self.status.status = .noNetWork
            self.progressUI()
            self.startTimerService()
        } else {
            haveNetWorkProgressPing()
        }
    }

    /// 监听到有网络之后-ping处理[ping成功：结束；ping失败：开启计时器]
    func haveNetWorkProgressPing() {
        IIHTTPNetWork.getNetWorkStatusAndTimeLineWithPing { (status) in
            if status == .noNetWork {
                self.startTimerService()
            }
            self.status.status = status
            self.progressUI()
        }
    }

    /// 处理状态之后判定次数并发送通知
    public func progressUI() {
        if stateVW == nil {
            stateVW = IIAPPNoNetWorkVw(frame: CGRect.zero, status: self.status.status)
        } else {
            stateVW?.setState(state: self.status.status)
        }
        NotificationCenter.default.post(name: NSNotification.Name(IIAPPWebStatusNoti.IIAPPWebStatusChangeNotiName), object: nil, userInfo: ["stateVw": stateVW!, "state": self.status.status.rawValue])
    }
}
