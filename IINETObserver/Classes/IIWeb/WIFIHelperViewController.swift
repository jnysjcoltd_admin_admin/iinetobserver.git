//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// WIFIHelperViewController.swift
//
// Created by    Noah Shan on 2018/10/24
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import UIKit
import WebKit

/*
 打开外放
 FEATHRE:
 0.进入此页面先加载一个告知用户无网络&加载小助手的提示页面&小菊花转起来非hud提示加载信息中
 1.进入此页面访问某个网页，等待返回code=4
 2.等待小助手页面完全加载后[小菊花2S不转便判定加载完成]，显示出webview信息
 */

class WIFIHelperViewController: BaseViewController, WKNavigationDelegate {

    var web: WKWebView?

    let naviTitleTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_connect_wifihelper_title.rawValue)

    let hudTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_connect_wifihelper_wait.rawValue)

    /// 入参
    var wifiHelperURL: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = naviTitleTxt
        self.view.backgroundColor = UIColor.white
        self.addNavLeftButtonNormalImage(UIImage(named: "back_before"), navLeftButtonSelectedImage: UIImage(named: "back_after"))
        createVw()
    }

    /// 加载webview
    func createVw() {
        self.web = WKWebView()
        self.web?.navigationDelegate = self
        self.view.addSubview(web!)
        self.web?.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        })
        loadAlert()
        if let realUrl = wifiHelperURL {
            self.web?.load(URLRequest(url: realUrl))
        }
    }

    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        loadAlert()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ProgressHUD.shareInstance().remove()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ProgressHUD.shareInstance().remove()
    }

    private func loadAlert() {
        ProgressHUD.shareInstance().showProgress(withMessage: hudTxt)
    }
}
