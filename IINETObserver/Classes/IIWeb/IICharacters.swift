//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IICharacters.swift
//
// Created by    Noah Shan on 2018/10/24
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import UIKit
@_exported import IISwiftBaseUti
@_exported import IIHTTPRequest
@_exported import SnapKit
@_exported import IIUIAndBizConfig
@_exported import IIBaseComponents
@_exported import IIAOPNBP
@_exported import IIOCUtis


public class IICharacters: NSObject {

    /// IIAPIStruct().wifiHelperURL
    static var wifiHelperURL: String {
        return wifiHelperURLAction?() ?? ""
    }

    @objc public static var wifiHelperURLAction: (() -> String)?

    /// IIAPIStruct().pintFirstCheckAdd
    static var pintFirstCheckAdd: String {
        return pintFirstCheckAddAction?() ?? ""
    }

    @objc public static var pintFirstCheckAddAction: (() -> String)?

    /// IIAPIStruct().pingDoubleCheckAdd
    static var pingDoubleCheckAdd: String {
        return pingDoubleCheckAddAction?() ?? ""
    }

    @objc public static var pingDoubleCheckAddAction: (() -> String)?

    /// IIAPIStruct().netCheckheartAPI
    static var netCheckheartAPI: String {
        return netCheckheartAPIAction?() ?? ""
    }

    @objc public static var netCheckheartAPIAction: (() -> String)?
}
