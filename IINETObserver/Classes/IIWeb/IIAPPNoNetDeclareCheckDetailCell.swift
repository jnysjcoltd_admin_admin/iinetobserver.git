//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareCheckDetailCell.swift
//
// Created by    Noah Shan on 2018/11/22
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

class IIAPPNoNetDeclareCheckDetailCell: UITableViewCell {

    let host = UILabel()

    let indic = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)

    let resultVw = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        createVw()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // 65 x 50   width : 105
    func createVw() {
        self.addSubview(host)
        host.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(0)
            make.height.equalTo(20)
        }
        host.font = APPUIConfig.uiFont(with: 15)
        host.textColor = APPUIConfig.cloudThemeColor
        self.addSubview(indic)
        indic.snp.makeConstraints { (make) in
            make.left.equalTo(host.snp.right).offset(8)
            make.centerY.equalTo(host.snp.centerY)
        }
        indic.startAnimating()
        self.addSubview(resultVw)
        resultVw.snp.makeConstraints { (make) in
            make.left.equalTo(indic.snp.left)
            make.centerY.equalTo(self.snp.centerY)
            make.height.equalTo(12)
            make.width.equalTo(12)
        }
        resultVw.alpha = 0
        resultVw.setImage(UIImage(named: "iinet_check_host_fail"), for: UIControl.State.normal)
        resultVw.setImage(UIImage(named: "iinet_check_host_success"), for: UIControl.State.selected)
    }

    func setData(hostInfo: String) {
        host.snp.remakeConstraints { (make) in
            make.left.equalTo(0)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(IITextAndFontExtension().textLength(text: hostInfo, font: APPUIConfig.uiFont(with: 15)))
            make.height.equalTo(20)
        }
        host.text = hostInfo
    }

    /// 停止动画，添加结果view
    func stopAni(success: Bool) {
        self.indic.stopAnimating()
        self.indic.alpha = 0
        self.resultVw.alpha = 1
        self.resultVw.isSelected = success
    }

}
