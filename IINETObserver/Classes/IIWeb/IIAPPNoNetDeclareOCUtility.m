//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareOCUtility.m
//
// Created by    Noah Shan on 2018/11/13
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *


#import "IIAPPNoNetDeclareOCUtility.h"
#import "resolv.h"
#include <arpa/inet.h>
/// 需求： libresolv.dylib

@implementation IIAPPNoNetDeclareOCUtility


    /// 获取本机DNS服务器
- (NSString *)outPutDNSServers
    {
        res_state res = malloc(sizeof(struct __res_state));

        int result = res_ninit(res);

        NSMutableArray *dnsArray = @[].mutableCopy;

        if ( result == 0 )
        {
            for ( int i = 0; i < res->nscount; i++ )
            {
                NSString *s = [NSString stringWithUTF8String :  inet_ntoa(res->nsaddr_list[i].sin_addr)];

                [dnsArray addObject:s];
            }
        }
        else{
            NSLog(@"%@",@" res_init result != 0");
        }

        res_nclose(res);

        return dnsArray.firstObject;
    }

@end
