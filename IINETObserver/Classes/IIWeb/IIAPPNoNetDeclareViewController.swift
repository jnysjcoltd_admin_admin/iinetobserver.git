//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareViewController.swift
//
// Created by    Noah Shan on 2018/10/25
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import UIKit

enum HardCheckType: Int {
    case hard
    case wifi
    case host
}

class OBCheckModel {
    var title: String?
    var subTitle: String?
    var imgUrl: String?
    var type: HardCheckType?
}


/// 网络状态检查页面
open class IIAPPNoNetDeclareViewController: UIViewController {

    var titleTxt = getI18NStr(key: III18NEnum.iinetwork_ob_nonetworkvc_title.rawValue)

    var tab = UITableView()

    var cellModels: [OBCheckModel] = []

    public var hostArr = [("www.baidu.com", getI18NStr(key: III18NEnum.iinetwork_check_host_baidu.rawValue)),
                   ("www.aliyun.com", getI18NStr(key: III18NEnum.iinetwork_check_host_ali.rawValue)),
                   (IICharacters.netCheckheartAPI, getI18NStr(key: III18NEnum.iinetwork_check_host_cloud.rawValue))]

    /// 网络检测工具类
    let uti: IIAPPNoNetDeclareUtility = IIAPPNoNetDeclareUtility()

    /// 小助手页面是否可以跳转
    var couldJumpWifihelperVC: Bool = false

    /// 配置页面是否可以跳转
    var couldJumpHardwareVC: Bool = false

    /// 小助手url
    var wifiHelperURL: URL?

    /// analyze方法flag: 此方法不能连续执行，因为对tab进行了操作，所以添加这个属性控制
    var analyzeFlag: Bool = true

    public override func viewDidLoad() {
        super.viewDidLoad()
        initSelfNavi()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        initData()

        self.view.backgroundColor = APPUIConfig.bgLightGray
        self.title = titleTxt

        let item = UIBarButtonItem(image: UIImage(named: "back_before"), style: .plain, target: self, action: #selector(backButtonClick))
        self.navigationItem.leftBarButtonItem = item

        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationBecomeActive(noti:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        createVw()
    }

    func initData() {
        let modelone = OBCheckModel()
        modelone.type = .hard
        modelone.imgUrl = "network_hardware_progress"
        modelone.title = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_hardware.rawValue)
        modelone.subTitle = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_hardware_detail.rawValue)
        self.cellModels.append(modelone)
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        analyzeNetwork()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tab.reloadData()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func createVw() {
        self.view.addSubview(tab)
        tab.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        tab.delegate = self
        tab.dataSource = self
        tab.separatorStyle = .none
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    /// 检测网络问题
    func analyzeNetwork() {
        if !analyzeFlag {
            return
        }
        analyzeFlag = false
        var itemIdx = self.cellModels.count - 1
        while itemIdx > 0 {
            self.cellModels.remove(at: itemIdx)
            self.tab.deleteRows(at: [IndexPath(row: itemIdx, section: 0)], with: UITableView.RowAnimation.none)
            itemIdx -= 1
        }

        uti.analyzeNetWork { [weak self](isConnect, isWifi) in
            if let cell = self?.tab.cellForRow(at: IndexPath(row: 0, section: 0)) as? IIAPPNoNetDeclareTabCell {
                self?.couldJumpHardwareVC = !isConnect
                cell.stopAni(success: isConnect)
            }
            self?.progressUI(connect: isConnect, isWifi: isWifi)
            self?.analyzeFlag = true
        }
    }

    /// 检测硬件结束后UI处理
    func progressUI(connect: Bool, isWifi: Bool) {
        if isWifi {
            let model = OBCheckModel()
            model.imgUrl = "wifi_router_progress"
            model.title = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_wifihelper.rawValue)
            model.subTitle = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_wifihelper_detail.rawValue)
            model.type = .wifi
            self.cellModels.append(model)
            checkWifi()
        }
        if connect {
            let model = OBCheckModel()
            model.imgUrl = "wifi_router_progress"
            model.title = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_dnsservice.rawValue)
            model.subTitle = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork_check_dnsservice_detail.rawValue)
            model.type = .host
            self.cellModels.append(model)
            checkHost()
        }
        var indexs: [IndexPath]?
        if self.cellModels.count == 2 {
            indexs = [IndexPath(row: 1, section: 0)]
        } else if self.cellModels.count == 3 {
            indexs = [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0)]
        }
        guard let realIndex = indexs else { return }
        self.tab.insertRows(at: realIndex, with: UITableView.RowAnimation.none)
    }

    /// 检测wifi
    private func checkWifi() {
        uti.analyzeWifiHelper { [weak self](optionalURL, wifiType) in
            guard let realModels = self?.cellModels else { return }
            var indexs: IndexPath?
            for i in 0 ..< realModels.count {
                if realModels[i].type == .wifi {
                    indexs = IndexPath(row: i, section: 0)
                }
            }
            guard let realIndex = indexs else { return }
            if let cell = self?.tab.cellForRow(at: realIndex) as? IIAPPNoNetDeclareTabCell {
                if wifiType == .ok {
                    cell.stopAni(success: true)
                } else if wifiType == .wifiHelper {
                    self?.couldJumpWifihelperVC = true
                    self?.wifiHelperURL = optionalURL!
                    cell.stopAni(success: false)
                } else {
                    cell.stopAni(success: false, type: .wifiError)
                }
            }
        }
    }

    /// 检测域名
    private func checkHost() {
        func getIndex(with host: String) -> Int {
            for i in 0 ..< self.hostArr.count {
                if host == hostArr[i].0 {
                    return i
                }
            }
            return -1
        }
        for i in self.hostArr {
            uti.analyzeHighHost(host: i.0) { [weak self](result) in
                guard let realModels = self?.cellModels else { return }
                var indexs: IndexPath?
                for i in 0 ..< realModels.count {
                    if realModels[i].type == .host {
                        indexs = IndexPath(row: i, section: 0)
                    }
                }
                guard let realIndex = indexs else { return }
                if let cell = self?.tab.cellForRow(at: realIndex) as? IIAPPNoNetDeclareCheckCell {
                    let index = getIndex(with: i.0)
                    cell.stopAni(success: result, row: index)
                }
            }
        }
    }
}

extension IIAPPNoNetDeclareViewController {
    @objc func applicationBecomeActive(noti: Notification) {
        self.tab.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)
        analyzeNetwork()
    }
}

extension IIAPPNoNetDeclareViewController: UITableViewDelegate, UITableViewDataSource {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellModels.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let resusid = "IIAPPNoNetDeclareViewControllerReuseCellID"
        if self.cellModels[indexPath.row].type == .host {
            let cell = IIAPPNoNetDeclareCheckCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: resusid)
            cell.setData(txt: self.cellModels[indexPath.row].title ?? "", imgUrl: self.cellModels[indexPath.row].imgUrl ?? "", arrInfos: hostArr)
            return cell
        }
        let types = self.cellModels[indexPath.row].type == .hard ? true : false
        let cell = IIAPPNoNetDeclareTabCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: resusid, isRepire: types)
        cell.setData(txt: self.cellModels[indexPath.row].title ?? "", subTitle: self.cellModels[indexPath.row].subTitle ?? "", imgUrl: self.cellModels[indexPath.row].imgUrl ?? "")
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.cellModels[indexPath.row].type == .host {
            return 150
        }
        let width = IITextAndFontExtension().textLength(text: self.cellModels[indexPath.row].subTitle ?? "", font: APPUIConfig.uiFont(with: 15))
        let realHeight = IITextAndFontExtension().progressRealHeight(textLength: width, eachLineWidth: (APPUIConfig.aWeight - 105), eachLineHeight: 20)
        return 110 + realHeight
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cellModels[indexPath.row].type == .wifi && couldJumpWifihelperVC {
            let con = WIFIHelperViewController()
            con.wifiHelperURL = self.wifiHelperURL
            self.navigationController?.pushViewController(con, animated: true)
        }
        if self.cellModels[indexPath.row].type == .hard && couldJumpHardwareVC {
            let con = IIAPPNoNetHardwarecheckViewController()
            self.navigationController?.pushViewController(con, animated: true)
        }
    }

    @objc func backButtonClick() {
        self.navigationController?.popViewController(animated: true)
    }

    /// 导航栏初始化
    func initSelfNavi() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 15/255, green: 120/255, blue: 205/255, alpha: 1)//IHTUIConfig.mainThemeColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}
