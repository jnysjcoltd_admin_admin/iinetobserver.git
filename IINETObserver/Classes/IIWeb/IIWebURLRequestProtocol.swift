//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIWebURLRequest.swift
//
// Created by    Noah Shan on 2018/10/31
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

/*
 FEATURE:
 1.在原有的oc  urlprotocol基础上添加
 2.设置webview-request请求缓存
    a.在缓存过程中数据丢失忽略
    b.此处的日志数据完整性要求不高
 3.canInitWithRequest必须要实现的方法返回true表示protocol需要处理这个request

 */
class IIWebURLRequestProtocol: NSObject {

    private static var shareInstance: IIWebURLRequestProtocol!

    /// 缓存数据数组
    @objc var requestDataMemCacheArr: [WebVWEvent] = []

    private override init() {
        super.init()
    }

    @objc public static func getInstance() -> IIWebURLRequestProtocol {
        if shareInstance == nil {
            shareInstance = IIWebURLRequestProtocol()
        }
        return shareInstance
    }

    /// 对外暴露方法
    @objc public func write2Log(urlStr: String, milSecs: Int) {
//        let event = WebVWEvent()
//        event.setBaseInfo(apiName: urlStr, time: Date(), milSecs: milSecs)
//        AOPDiskIOProgress.getInstance().writeEventsToDisk(with: [NSUUID().uuidString: [event]], wholeContent: false)
    }

}
