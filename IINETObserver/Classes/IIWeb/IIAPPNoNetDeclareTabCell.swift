//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareTabCell.swift
//
// Created by    Noah Shan on 2018/10/29
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

class IIAPPNoNetDeclareTabCell: UITableViewCell {

    let topImg: UIImageView = UIImageView()

    let nameLb: UILabel = UILabel()

    let subTitleLb: UILabel = UILabel()

    let indic = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)

    let resultVw: UIButton = UIButton()

    let repireVw: UILabel = UILabel()

    //"去修复"
    let repireTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_check_repiretxt.rawValue)

    // 去认证
    let repireTxtDe: String = getI18NStr(key: III18NEnum.iinetwork_ob_check_hardwaresetting_certification.rawValue)

    let repireImg: UIImageView = UIImageView()

    var txtIsRepire: Bool = false

    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, isRepire: Bool) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        txtIsRepire = isRepire
        createVw()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // 65 x 50   width : 105
    func createVw() {
        self.addSubview(topImg)
        self.addSubview(nameLb)
        self.addSubview(indic)
        self.addSubview(resultVw)
        self.addSubview(subTitleLb)
        self.addSubview(repireVw)
        self.addSubview(repireImg)
        //图片
        topImg.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.equalTo(30)
            make.width.equalTo(38)
            make.height.equalTo(38)
        }
        //title
        nameLb.snp.makeConstraints { (make) in
            make.left.equalTo(topImg.snp.right).offset(35)
            make.top.equalTo(topImg.snp.top)
            make.height.equalTo(25)
            make.right.equalTo(-16)
        }
        nameLb.font = APPUIConfig.uiFont(with: 17)
        nameLb.textColor = APPUIConfig.mainCharColor
        indic.startAnimating()
        //subtitle
        subTitleLb.snp.makeConstraints { (make) in
            make.left.equalTo(nameLb.snp.left)
            make.top.equalTo(nameLb.snp.bottom).offset(10)
            //make.bottom.equalTo(-20)
            make.right.equalTo(-16)
        }
        subTitleLb.numberOfLines = 0
        subTitleLb.font = APPUIConfig.uiFont(with: 15)
        subTitleLb.textColor = APPUIConfig.charGrayColor
        //小菊花
        indic.snp.makeConstraints { (make) in
            make.left.equalTo(nameLb.snp.left)
            make.bottom.equalTo(subTitleLb.snp.bottom).offset(10)
        }
        //result vw
        resultVw.snp.makeConstraints { (make) in
            make.left.equalTo(nameLb.snp.left)
            make.bottom.equalTo(indic.snp.bottom)
            make.height.equalTo(25)
            make.width.equalTo(25)
        }
        resultVw.alpha = 0
        resultVw.setImage(UIImage(named: "iiweb_progress_result_fail"), for: UIControl.State.normal)
        resultVw.setImage(UIImage(named: "progress_result_success"), for: UIControl.State.selected)
        //修复文案
        repireVw.snp.makeConstraints { (make) in
            make.left.equalTo(resultVw.snp.right).offset(8)
            make.centerY.equalTo(resultVw.snp.centerY)
            make.width.equalTo(50)
            make.height.equalTo(25)
        }
        repireVw.font = APPUIConfig.uiFont(with: 15)
        repireVw.textColor = UIColor.red
        repireVw.alpha = 0
        repireVw.text = txtIsRepire ? repireTxt : repireTxtDe
        //修复Jian箭头
        repireImg.snp.makeConstraints { (make) in
            make.left.equalTo(repireVw.snp.right)
            make.centerY.equalTo(resultVw.snp.centerY)
            make.width.equalTo(8)
            make.height.equalTo(12)
        }
        repireImg.alpha = 0
        repireImg.image = UIImage(named: "iinet_check_repire_arrow")
        //line
        let lineVw = UIView()
        self.addSubview(lineVw)
        lineVw.snp.makeConstraints { (make) in
            make.left.equalTo(resultVw.snp.left)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(1)
        }
        lineVw.backgroundColor = APPUIConfig.lineLightGray
    }

    func setData(txt: String, subTitle: String, imgUrl: String) {
        self.nameLb.text = txt
        self.subTitleLb.text = subTitle
        self.topImg.image = UIImage(named: imgUrl)
        //
        let width = IITextAndFontExtension().textLength(text: subTitle, font: APPUIConfig.uiFont(with: 15))
        let realHeight = IITextAndFontExtension().progressRealHeight(textLength: width, eachLineWidth: (APPUIConfig.aWeight - 105), eachLineHeight: 20) + 10
        self.indic.snp.remakeConstraints { (make) in
            make.left.equalTo(nameLb.snp.left)
            make.top.equalTo(subTitleLb.snp.top).offset(realHeight)
        }
    }

    /// 停止动画，添加结果view; type 为 .wifiError的时候，wifi有故障但是不需要认证
    func stopAni(success: Bool, type: WiFiErrorType? = nil) {
        self.indic.stopAnimating()
        self.indic.alpha = 0
        self.resultVw.alpha = 1
        self.resultVw.isSelected = success
        self.repireVw.alpha = success ? 0 : 1
        self.repireImg.alpha = success ? 0 : 1
        if type == .wifiError {
            self.repireVw.alpha = 0
            self.repireImg.alpha = 0
        }
    }
    
}
