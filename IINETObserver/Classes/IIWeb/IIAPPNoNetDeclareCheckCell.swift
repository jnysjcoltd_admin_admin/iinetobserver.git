//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareCheckCell.swift
//
// Created by    Noah Shan on 2018/11/22
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

class IIAPPNoNetDeclareCheckCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    let topImg: UIImageView = UIImageView()

    let nameLb: UILabel = UILabel()

    let contentTab: UITableView = UITableView()

    var hostArr = [(String, String)]()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        createVw()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // 65 x 50   width : 105
    func createVw() {
        self.addSubview(topImg)
        self.addSubview(nameLb)
        self.addSubview(contentTab)
        //图片
        topImg.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.equalTo(30)
            make.width.equalTo(38)
            make.height.equalTo(38)
        }
        //title
        nameLb.snp.makeConstraints { (make) in
            make.left.equalTo(topImg.snp.right).offset(35)
            make.top.equalTo(topImg.snp.top)
            make.height.equalTo(25)
            make.right.equalTo(-16)
        }
        nameLb.font = APPUIConfig.uiFont(with: 17)
        nameLb.textColor = APPUIConfig.mainCharColor
        //tab
        self.contentTab.delegate = self
        self.contentTab.dataSource = self
        self.contentTab.separatorStyle = .none
        self.contentTab.snp.makeConstraints { (make) in
            make.left.equalTo(nameLb.snp.left)
            make.right.equalTo(0)
            make.top.equalTo(nameLb.snp.bottom).offset(10)
            make.height.equalTo(CGFloat(hostArr.count) * 30 + CGFloat(hostArr.count - 1) * 10)
        }
        self.contentTab.bounces = false
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = IIAPPNoNetDeclareCheckDetailCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "IIAPPNoNetDeclareCheckCellreuseid")
        cell.setData(hostInfo: self.hostArr[indexPath.row].1)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hostArr.count
    }

    func setData(txt: String, imgUrl: String, arrInfos: [(String, String)]) {
        self.hostArr = arrInfos
        self.contentTab.reloadData()
        self.nameLb.text = txt
        self.topImg.image = UIImage(named: imgUrl)
        self.contentTab.snp.updateConstraints { (make) in
            make.height.equalTo(CGFloat(hostArr.count) * 30 + CGFloat(hostArr.count - 1) * 10)
        }
    }

    /// 停止动画，添加结果view
    func stopAni(success: Bool, row: Int) {
        if row < 0 { return }
        if let cell = self.contentTab.cellForRow(at: IndexPath(row: row, section: 0)) as? IIAPPNoNetDeclareCheckDetailCell {
            cell.stopAni(success: success)
        }
    }

}
