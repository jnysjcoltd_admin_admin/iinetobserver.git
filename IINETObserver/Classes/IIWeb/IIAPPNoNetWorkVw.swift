//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetWorkVw.swift
//
// Created by    Noah Shan on 2018/10/25
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

/// 弹出的提示网络view
public class IIAPPNoNetWorkVw: UIView {

    var txtLb: UILabel = UILabel()

    var img: UIImageView = UIImageView()

    var pingState: PingNetWorkStatus = PingNetWorkStatus.goodWork

    public init(frame: CGRect, status: PingNetWorkStatus) {
        super.init(frame: frame)
        self.pingState = status
        createVw()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init IIAPPNoNetWorkVw error ")
    }

    func createVw() {
        self.frame.size = CGSize(width: APPUIConfig.aWeight, height: 44)

        let bgVw: UIView = UIView()
        self.addSubview(bgVw)
        bgVw.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(-0.5)
        }
        bgVw.backgroundColor = UIColor(red: 255 / 255, green: 220 / 255, blue: 220 / 255, alpha: 1)
        bgVw.addSubview(txtLb)
        bgVw.addSubview(img)
        img.image = UIImage(named: "webex_networkFail")
        img.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.width.equalTo(25)
            make.height.equalTo(25)
            make.centerY.equalTo(self.snp.centerY)
        }
        txtLb.snp.makeConstraints { (make) in
            make.left.equalTo(img.snp.right).offset(10)
            make.right.equalTo(-16)
            make.centerY.equalTo(self.snp.centerY)
            make.height.equalTo(20)
        }
        txtLb.font = APPUIConfig.uiFont(with: 15)
        txtLb.textColor = APPUIConfig.mainCharColor
        setState(state: self.pingState)
        self.tapActionsGesture { [weak self] in
            if self?.pingState == .goodWork || self?.pingState == .badNetWork { return }
            let con = IIAPPNoNetDeclareViewController()
            con.hidesBottomBarWhenPushed = true
            self?.iiViewController()?.navigationController?.pushViewController(con, animated: true)
        }
        //arrow
        let arrowImg = UIImageView()
        self.addSubview(arrowImg)
        arrowImg.snp.makeConstraints { (make) in
            make.right.equalTo(-16)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(8)
            make.height.equalTo(14)
        }
        arrowImg.image = UIImage(named: "iiweb_ob_checknet")
        let botLine = UIView()
        self.addSubview(botLine)
        botLine.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(0.5)
            make.bottom.equalTo(0)
        }
        botLine.backgroundColor = APPUIConfig.bgLightGray
    }

    func setState(state: PingNetWorkStatus) {
        self.alpha = 1
        self.pingState = state
        switch self.pingState {
        case .badNetWork :
            self.txtLb.text = getI18NStr(key: III18NEnum.iinetwork_ob_badnetwork.rawValue)
            self.alpha = 0
        case .noNetWork :
            self.txtLb.text = getI18NStr(key: III18NEnum.iinetwork_ob_nonetwork.rawValue)
        case .goodWork :
            self.txtLb.text = ""
            self.alpha = 0
//        case .wifiHelper :
//            self.txtLb.text = "网络连接发生问题，请检查你的网络设置"
        }
    }

    func copy(with zone: NSZone? = nil) -> Any? {
        guard let newVw = type(of: self).init() as? IIAPPNoNetWorkVw else { return nil }
        newVw.txtLb.text = self.txtLb.text
        newVw.img = UIImageView(image: self.img.image)
        newVw.alpha = self.alpha
        newVw.pingState = self.pingState
        return newVw
    }

    @objc public func iicopy() -> IIAPPNoNetWorkVw {
        let vw = IIAPPNoNetWorkVw(frame: self.frame, status: self.pingState)
        return vw
    }

}
