//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetDeclareUtility.swift
//
// Created by    Noah Shan on 2018/10/29
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import Foundation

enum WiFiErrorType: Int {
    /// 有网
    case ok = 0
    /// wifi需要认证
    case wifiHelper = 1
    /// 假的WIFI 但是不需要认证
    case wifiError = 2
}

class IIAPPNoNetDeclareUtility: NSObject {

    override init() {
        super.init()
    }

    /// 判定硬件是否有链接 true 有 false 没有
    func analyzeNetWork(resultAction:@escaping (_ result: Bool, _ isWifi: Bool) -> Void) {
        GCDUtils.delayProgress(delayTime: 1) {
            let status = IIHTTPNetWorkUtility().analyzeWifiOrOthers()
            if status == .noNetWork{
                resultAction(false, false)
            } else {
                resultAction(true, status == .wifi)
            }
        }
    }

    /// 弃用
    /// false - 有问题： true: 没问题
    /// 检测dns是否有效-先ping域名,之后ping ip如果结果相同则dns没问题，如果不相同，dns有问题
    func analyzeDNSService(action:@escaping (_ result: Bool) -> Void) {
//        GCDUtils.delayProgress(delayTime: 1) {
//            //let dnsService = IIAPPNoNetDeclareOCUtility().outPutDNSServers()
//            IIHTTPNetWork.getNetWorkStatusAndTimeLineWithPing { (status) in
//                //先更改ping地址为IP地址
//                RealReachability.sharedInstance().hostForPing = IIAPIStruct().pingIPAddress
//                RealReachability.sharedInstance().hostForCheck = IIAPIStruct().pingIPAddress
//                IIHTTPNetWork.getNetWorkStatusAndTimeLineWithPing(connectAction: { (ipStatus) in
//                    //还原ping地址
//                    RealReachability.sharedInstance().hostForPing = IIAPIStruct().pingBaiDu
//                    RealReachability.sharedInstance().hostForCheck = IIAPIStruct().pingALICloud
//                    if (status == PingNetWorkStatus.noNetWork && ipStatus != PingNetWorkStatus.noNetWork) || (status != PingNetWorkStatus.noNetWork && ipStatus == PingNetWorkStatus.noNetWork){
//                        action(false)
//                    }else{
//                        action(true)
//                    }
//                })
//            }
//        }
    }

    /// 检测wifi助手[网络不连通并且是wifi环境下再去检测]
    func analyzeWifiHelper(wifiHelperResult: @escaping(_ url: URL?, _ type: WiFiErrorType) -> Void) {
        GCDUtils.delayProgress(delayTime: 1) {
            let currentStatus = IIHTTPNetWorkUtility().analyzeWifiOrOthers()
            if currentStatus != HardWareNetWorkStatus.wifi {
                wifiHelperResult(nil, .ok)
                return
            }
            IIHTTPRequest.startRequest(showAlertInfo: false, shouldGetRedirect: true, method: IIHTTPMethod.get, url: IICharacters.wifiHelperURL, params: nil, successAction: { (_) in
                wifiHelperResult(nil, .ok)
            }) { (errorInfo) in
                if errorInfo.wifiHelperURL != nil {
                    wifiHelperResult(errorInfo.wifiHelperURL!, .wifiHelper)
                } else if errorInfo.errorType == ERRORMsgType.noConnection || errorInfo.errorType == ERRORMsgType.timeOut {
                    wifiHelperResult(nil, .wifiError)
                } else {
                    wifiHelperResult(nil, .ok)
                }
            }
        }
    }

    /// 检测高级域名-是否可以ping通某个高级域名[结束之后切换回原来的域名]
    /// 检测到如果有host则进行接口请求处理
    func analyzeHighHost(host: String, action:@escaping (_ result: Bool) -> Void) {
        guard let url = URL(string: host) else { return }
        if url.host != nil {
            IIHTTPRequest.startRequest(showAlertInfo: false, method: IIHTTPMethod.get, url: host, params: nil, successAction: { (_) in
                action(true)
            }) { (errorInfo) in
                if errorInfo.errorType == ERRORMsgType.timeOut || errorInfo.errorType == ERRORMsgType.noConnection {
                    action(false)
                } else {
                    action(true)
                }
            }
            return
        }
        GCDUtils.delayProgress(delayTime: 1) {
            GCDUtils.limitqueue.async {
                GCDUtils.semap.wait()
                RealReachability.sharedInstance().hostForPing = host
                RealReachability.sharedInstance().hostForCheck = host
                IIHTTPNetWork.getNetWorkStatusAndTimeLineWithPing { (status) in
                    GCDUtils.toMianThreadProgressSome {
                        if status == PingNetWorkStatus.noNetWork {
                            action(false)
                        } else {
                            action(true)
                        }
                    }
                    RealReachability.sharedInstance().hostForPing = IICharacters.pintFirstCheckAdd
                    RealReachability.sharedInstance().hostForCheck = IICharacters.pingDoubleCheckAdd
                    GCDUtils.semap.signal()
                }
            }
        }
    }
}
