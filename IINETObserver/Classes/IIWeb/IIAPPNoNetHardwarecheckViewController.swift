//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *
//
// IIAPPNoNetHardwarecheckViewController.swift
//
// Created by    Noah Shan on 2018/11/22
// InspurEmail   shanwzh@inspur.com
//
// Copyright © 2018年 Inspur. All rights reserved.
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * *

import UIKit

/// 硬件连通性说明view
class IIAPPNoNetHardwarecheckViewController: UIViewController {

    //"硬件连接诊断"
    let titleInfo = getI18NStr(key: III18NEnum.iinetwork_ob_check_repirehardware.rawValue)

    //"建议按照以下方法检查网络硬件连接"
    let titleTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_check_hardwaretxt.rawValue)

    //"1.打开手机“设置”并把“Wi-Fi”开关保持开启状态。"
    let subOneTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_check_hardwaresubone.rawValue)

    //"2.打开手机“设置”-“通用”-“蜂窝移动网络”，并把“蜂窝移动数据”开关保持开启状态。"
    let subTwoTxt: String = getI18NStr(key: III18NEnum.iinetwork_ob_check_hardwaresubtwo.rawValue)

    //"去设置"
    let btnTxt = getI18NStr(key: III18NEnum.iinetwork_ob_check_hardwaresetting.rawValue)

    // 设置按钮
    let checkBtn = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        initSelfNavi()
        createNavi()
        createVw()
        talkJoke2Apple()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func createNavi() {
        self.title = titleInfo
        self.view.backgroundColor = UIColor.white

        let item = UIBarButtonItem(image: UIImage(named: "back_before"), style: .plain, target: self, action: #selector(backButtonClick))
        self.navigationItem.leftBarButtonItem = item

        //self.view.effectiveUserInterfaceLayoutDirection
        self.edgesForExtendedLayout = UIRectEdge.bottom
    }

    func createVw() {
        let title = UILabel()
        self.view.addSubview(title)
        title.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.top.equalTo(30)
            //make.height.equalTo(30)
        }
        title.numberOfLines = 0
        title.font = APPUIConfig.boldUIFont(with: 18)
        title.text = self.titleTxt
        title.textColor = APPUIConfig.mainCharColor
        //
        let subOne = UILabel()
        self.view.addSubview(subOne)
        subOne.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.top.equalTo(title.snp.bottom).offset(30)
        }
        subOne.numberOfLines = 0
        subOne.font = APPUIConfig.uiFont(with: 16)
        subOne.text = self.subOneTxt
        subOne.textColor = APPUIConfig.charGrayColor
        //
        let subTwo = UILabel()
        self.view.addSubview(subTwo)
        subTwo.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.top.equalTo(subOne.snp.bottom).offset(20)
        }
        subTwo.textColor = APPUIConfig.charGrayColor
        subTwo.numberOfLines = 0
        subTwo.font = APPUIConfig.uiFont(with: 16)
        subTwo.text = self.subTwoTxt
        //btn
        self.view.addSubview(checkBtn)
        checkBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(180)
            make.height.equalTo(40)
            make.top.equalTo(subTwo.snp.bottom).offset(100)
        }
        checkBtn.layer.cornerRadius = 4
        checkBtn.layer.masksToBounds = true
        checkBtn.layer.borderColor = APPUIConfig.cloudThemeColor.cgColor
        checkBtn.layer.borderWidth = 1
        checkBtn.setTitle(btnTxt, for: UIControl.State.normal)
        checkBtn.setTitleColor(APPUIConfig.cloudThemeColor, for: UIControl.State.normal)
        checkBtn.titleLabel?.font = APPUIConfig.uiFont(with: 20)
        checkBtn.tapActionsGesture {
            IIHardwareAuthRequest().openSysSettingPage()
        }
    }

    private func talkJoke2Apple() {
        if IMPUserModel.activeInstance()?.enterprise?.id == 9_998 {
            self.checkBtn.alpha = 0
        }
    }

    @objc func backButtonClick() {
        self.navigationController?.popViewController(animated: true)
    }

    /// 导航栏初始化
    func initSelfNavi() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 15/255, green: 120/255, blue: 205/255, alpha: 1)//IHTUIConfig.mainThemeColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}
