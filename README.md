# IINETObserver

[![CI Status](https://img.shields.io/travis/hatjs880328s/IINETObserver.svg?style=flat)](https://travis-ci.org/hatjs880328s/IINETObserver)
[![Version](https://img.shields.io/cocoapods/v/IINETObserver.svg?style=flat)](https://cocoapods.org/pods/IINETObserver)
[![License](https://img.shields.io/cocoapods/l/IINETObserver.svg?style=flat)](https://cocoapods.org/pods/IINETObserver)
[![Platform](https://img.shields.io/cocoapods/p/IINETObserver.svg?style=flat)](https://cocoapods.org/pods/IINETObserver)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IINETObserver is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IINETObserver'
```

## Author

hatjs880328s, shanwzh@inspur.com

## License

IINETObserver is available under the MIT license. See the LICENSE file for more info.
